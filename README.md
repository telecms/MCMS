<p align="center">
	<a href="https://www.mingsoft.net/"><img src="http://cdn.mingsoft.net/global/images/logo-blue.png"  width="50" height=""50></a>
</p>
<p align="center">
	<strong>Value comes from sharing</strong>
</p>
<p align="center">
	<a target="_blank" href="https://search.maven.org/search?q=ms-mcms">
        <img src="https://img.shields.io/maven-central/v/net.mingsoft/ms-mcms.svg?label=Maven%20Central" />
	</a>
	<a target="_blank" href="hhttps://baike.baidu.com/item/MIT%E8%AE%B8%E5%8F%AF%E8%AF%81/6671281?fr=aladdin">
        <img src="https://img.shields.io/:license-MIT-blue.svg" />
	</a>
	<a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
	<a target="_blank" href="https://gitee.com/mingSoft/MCMS/stargazers">
		<img src="https://gitee.com/mingSoft/MCMS/badge/star.svg?theme=dark" alt='gitee star'/>
	</a>
	<a target="_blank" href='https://github.com/ming-soft/mcms'>
		<img src="https://img.shields.io/github/stars/ming-soft/mcms.svg?style=social" alt="github star"/>
	</a>
	
</p>
<p align="center">
	<a href="https://www.mingsoft.net" target="_blank">Mingfei Platform</a>
	<a href="http://cms.demo.mingsoft.net/ms/login.do" target="_blank">Online demo</a>
	<a href="http://doc.mingsoft.net/mcms/" target="_blank">Online Manual</a>
	<a href="https://mingsoft.ke.qq.com/?tuin=383187f3#tab=1&category=-1" target="_blank">Tencent Classroom Online Video</a><br/>
	<a href="https://ke.qq.com/course/3026403" target="_blank">Code Generator Video Tutorial</a><br/>
	<a href="https://cloud.189.cn/t/I3maM3uURjmm" target="_blank">Windows one-click version\Linux one-click version</a>
</p>

-------------------------------------------------------------------------------

Many people say that Mingfei is the only complete open source J2EE system in the Great Heavenly Kingdom! The team hopes to see more excellent and easy-to-use open source systems, and we will continue to work hard! <br/>

QQ exchange group number: [![Join QQ group](https://img.shields.io/badge/wuqun-231211521-blue.svg)](https://jq.qq.com/?_wv=1027&k =5oF19sl) [![Join QQ group](https://img.shields.io/badge/four groups-881894877-blue.svg)](https://jq.qq.com/?_wv=1027&k=5oF19sl ) [![Join QQ group](https://img.shields.io/badge/group-231212174-blue.svg)](https://jq.qq.com/?_wv=1027&k=5zykX7V) [! [Join QQ group](https://img.shields.io/badge/two groups-221335098-blue.svg)](https://jq.qq.com/?_wv=1027&k=56BqFKu) [![Join QQ group](https://img.shields.io/badge/three groups-242805203-blue.svg)](https://jq.qq.com/?_wv=1027&k=5oF19sl)


# Open source description
* The system is 100% open source
* Modular development mode, the modules developed by Mingfei are released to the maven central library. You can pull the source code through the pom.xml file

```
<dependency>
	<groupId>net.mingsoft</groupId>
	<artifactId>模块</artifactId>
	<version>version number</version>
	<classifier>sources</classifier>
	<scope>provided</scope>
</dependency>
```
# commercial
Based on the [MIT](https://www.oschina.net/question/12_2829) open source agreement, it can be directly commercialized without authorization, but please respect the spirit of open source and do not remove Mingfei's comments and copyright information in the code


# Features
* Free and complete open source: Based on the MIT protocol, the source code is completely open source, without commercial restrictions, the MS development team promises to permanently open source the MCMS content system;<br/>
* Tag-based website construction: no professional background development skills are required, as long as you use the tags provided by the system, you can easily build a website;<br/>
* html static: The system supports the static of the whole site;<br/>
* Cross-terminal: The site supports PC and mobile access at the same time, and will automatically switch to the corresponding interface according to the terminal accessed, and the data is managed by the system;<br/>
* Massive templates: Mingfei shares more free and exquisite corporate website templates through MStore (MS mall) to reduce the cost of building a website;<br/>
* Rich plug-ins: In order to make MCms adapt to more business scenarios, users in MStore can download corresponding plug-ins, such as: station group plug-ins, WeChat plug-ins, mall plug-ins, etc.;<br/>
* Monthly update: The Mingfei team promises that the 28th of every month is the system upgrade day to share more useful templates and plug-ins;<br/>
* Rich documentation: In order to allow users to use the MCms system for development more quickly, the Mingfei team continues to update development-related documents, such as label documents, usage documents, video tutorials, etc.;<br/>
# object oriented
* Enterprises: help early-stage companies or teams to quickly build a product technology platform and speed up the company's project development progress;
* Developers: help developers to quickly complete outsourced projects and avoid building systems from scratch;
* Learners: Students who are beginners in JAVA can download the source code for learning and communication;

# development environment
It is recommended that developers use the following environment to avoid problems caused by the version
* Windows、Linux
* Eclipse、Idea
* Mysql≧5.7
* JDK≧8
* Tomcat≧8


# Quick experience (import into Eclipse or IDEA)

1. Check out the source code:
git clone https://gitee.com/mingSoft/MCMS.git<br/>
2. Import project<br/>
* Eclipse import, menu File -> Import, then select Maven -> Existing Maven Projects, click the Next> button, select the checked out project MCMS folder, and then click the Finish button to successfully import
* IDEA import, click Import Project, select the pom.xml file, click the Next button, select the Import Maven projects automatically check box, and then click the Next button until you click the Finish button, you can import successfully<br/>

4. Eclipse (IDEA) will automatically load the Maven dependency package, and the initial loading will be slow (depending on your own network conditions). If there is a small cross on the project, please open the Problems window and check the specific error content until there is no error < br/>
5. Create the database mcms (the database uses utf-8 encoding), import doc/mcms-version number.sql, if you upgrade the existing system, please use *-up-*.sql to upgrade, if you import the full version of SQL corresponding to the system, The sql upgrade patch does not need to be imported repeatedly;<br/>
6. Modify the database setting parameters in the src\main\resources\application-dev.yml file;<br/>
7. Run MSApplication.java main method<br/>
8. First, visit the background address: http://localhost:8080/ms/login.do, administrator account, username: msopen Password: msopen, enter the background and click Content Management -> Static Menu to "Generate Home Page" , "Generate Column", "Generate Article" once (attention!!! This is the background login interface, not the member center login interface)


# Technical selection

## Backend framework

| Technology | Name | Official Website |
| :--- | :--- | :--- |
| Spring Framework | Container | [http://projects.spring.io/spring-framework](http://projects.spring.io/spring-framework/) |
| Spring Boot | MVC Framework | [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot) |
| Apache Shiro | Security Framework | [http://shiro.apache.org](http://shiro.apache.org/) |
| Spring session | Distributed Session Management | [http://projects.spring.io/spring-session](http://projects.spring.io/spring-session) |
| MyBatis | ORM框架 | [http://www.mybatis.org](http://www.mybatis.org/mybatis-3/zh/index.html) |
| Freemarker | View Framework | [http://freemarker.foofun.cn](http://freemarker.foofun.cn/) |
| PageHelper | MyBatis Paging Plugin | [http://git.oschina.net/free/Mybatis\_PageHelper](http://git.oschina.net/free/Mybatis_PageHelper) |
| Log4J | Log Components | [http://logging.apache.org](http://logging.apache.org) |
| Maven | Project Build | [http://maven.apache.org](http://maven.apache.org/) |
| Elasticsearch | Distributed Search Engine | [https://www.elastic.co](https://www.elastic.co/) |
| Redis | Distributed Cache Database | [https://redis.io](https://redis.io) |
| hutool | Tools | [http://hutool.mydoc.io](http://hutool.mydoc.io) |

## Front-end framework

| Technology | Name | Official Website |
| :--- | :--- | :--- |
| VUE| MVVM Framework| [https://cn.vuejs.org//](https://cn.vuejs.org//) |
| Element UI| UI 库 | [https://element.eleme.cn/2.0/#/zh-CN](https://element.eleme.cn/2.0/#/zh-CN) |
| jQuery | Library | [http://jquery.com/](http://jquery.com/) |
| Waves | Click Effects Plugin | [https://github.com/fians/Waves/](https://github.com/fians/Waves) |
| validator | Validation library | [https://github.com/chriso/validator.js](https://github.com/chriso/validator.js) |
| animate | animation | [http://daneden.github.io/animate.css/](http://daneden.github.io/animate.css/) |
| icon | Small vector icons\(to be updated\) | [https://www.iconfont.cn/](https://www.iconfont.cn/) |


# file description
* doc project document folder, which contains database files
* src/main/java java source code
* The resource configuration file of the src/main/resources project
* src/main/webapp
* src/main/webapp/static static resource files, such as: js, css, image, and other third-party front-end plugin libraries
* The static page generated by src/main/webapp/html, the actual project needs to be deleted, but it is only provided to developers to quickly preview the generated static page
* src/main/webapp/templet template folder
* src/main/webapp/upload upload resource folder
* src/main/webapp/WEB-INF/manager backend view page
* LICENSE project agreement description
* README.md project documentation
* pom.xml dependency configuration file



# Documentation
* User Manual http://doc.mingsoft.net/mcms/

# About the release notes [more versions view](https://www.mingsoft.net/html/default/cms/banben/index.html)
1. The source code of the open source version is permanently released for free, and developers and enterprises can use it for free for life. Every month, the team will collect the problems of the open source system and update it on the 28th of each month;
2. The update frequency of the enterprise version and above is faster, and the functions are more than that of the open source version. At the same time, additional plug-ins will be provided according to different versions, and different versions will also provide different manual online services (service hours: 9:30-17: 30)

# Software screenshot

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121402_82521059_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/120836_8a943ad5_1795833.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/120927_8a3f93e3_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/120939_7e5281db_1795833.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/120951_9d3a50b2_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121003_9c68e090_1795833.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121020_34c8e0ab_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121033_b425e406_1795833.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121044_41cc0d00_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121059_48b4cd7b_1795833.png"/></td>
    </tr>
     <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121112_8581de9a_1795833.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0523/121122_8b513dd1_1795833.png"/></td>
    </tr>
</table>

# Mingfei Platform

The following functions are available for free on the platform https://www.mingsoft.net
We are amateurs doing open source, and we are serious about writing code. On the road of product development, we have been exploring, learning, and investing, hoping to provide more valuable services to more enterprises and developers.

## project management

<table>
	<tr>
		<td><img src="//www.mingsoft.net/upload/1/cms/content/1600746325257.gif"/></td>
	</tr>
	<tr>
		<td><img src="//www.mingsoft.net/upload/1/cms/content/1600746514131.gif"/></td>
	</tr>	
</table>

## Code generator

<table>
	<tr>
		<td><img src="//www.mingsoft.net/upload/1/cms/content/1600746629646.gif"/></td>
	</tr>
	<tr>
		<td><img src="//www.mingsoft.net/upload/1/cms/content/1600746692814.gif"/></td>
	</tr>	
</table>





